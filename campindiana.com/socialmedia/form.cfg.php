<?php exit(0); ?> { 
"settings":
{
	"data_settings" : 
	{
		"save_database" : 
		{
			"database" : "",
			"is_present" : false,
			"password" : "",
			"port" : 3306,
			"server" : "",
			"tablename" : "",
			"username" : ""
		},
		"save_file" : 
		{
			"filename" : "form-results.csv",
			"is_present" : false
		},
		"save_sqlite" : 
		{
			"database" : "socialmedia.dat",
			"is_present" : false,
			"tablename" : "socialmedia"
		}
	},
	"email_settings" : 
	{
		"auto_response_message" : 
		{
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>You got mail!</title></head>\n<body style=\"background-color: #f9f9f9; padding-left: 11%; padding-top: 7%; padding-right: 2%; max-width: 700px; font-family: Helvetica, Arial;\">\n<style type=\"text/css\">\nbody {background-color: #f9f9f9;padding-left: 110px;padding-top: 70px; padding-right: 20px;max-width:700px;font-family: Helvetica, Arial;}\np{font-size: 12px; color: #666666;}\nh2{font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\ntable{width:80%;}\ntd {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:10%; padding-right:5px;}\na:link {color:#666666; text-decoration:underline;} a:visited {color:#666666; text-decoration:none;} a:hover {color:#00A2FF;}\nb{font-weight: bold;}\n</style>\n<h2 style=\"font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;\">Thanks for taking the time to fill out the form. <br/>Here's a copy of what you submitted:</h2>\n<div>\n[_form_results_]\n</div>\n</body>\n</html>\n",
				"is_present" : false,
				"subject" : "Thank you for your submission"
			},
			"from" : "",
			"is_present" : false,
			"to" : ""
		},
		"notification_message" : 
		{
			"bcc" : "",
			"cc" : "tracey@markowich.com",
			"custom" : 
			{
				"body" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head><title>You got mail!</title></head>\n<body style=\"background-color: #f9f9f9; padding-left: 2%; padding-top: 7%; padding-right: 20px; max-width: 1400px; font-family: Helvetica, Arial;\">\n<style type=\"text/css\">\nbody {background-color: #f9f9f9;padding-left: 11%; padding-top: 7%; padding-right: 2%;max-width:1400px;font-family: Helvetica, Arial;}\np{font-size: 12px; color: #666666;}\nh1{font-size: 20px !important;color: #cccccc !important;margin:0px;}\nh2{font-size: 28px !important;color: #666666 ! important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\ntable{width:100%;}\ntd {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:10%; padding-right:5px;}\na:link {color:#666666; text-decoration:underline;} a:visited {color:#666666; text-decoration:none;} a:hover {color:#00A2FF;}\nb{font-weight: bold;}\n</style>\n\n\n<div>\n[_form_results_]\n<p style=\"font-size: 12px; color: #666666;\">\n\n</div>\n</body>\n</html>\n",
				"is_present" : true,
				"subject" : "Somebody filled out your social media form!"
			},
			"from" : "Newsletter@CampIndiana.com",
			"is_present" : true,
			"replyto" : "",
			"to" : "cabooselake@yahoo.com"
		}
	},
	"general_settings" : 
	{
		"colorboxautoenabled" : false,
		"colorboxautotime" : 3,
		"colorboxenabled" : false,
		"colorboxname" : "Default",
		"formname" : "SocialMedia",
		"is_appstore" : "0",
		"timezone" : "UTC"
	},
	"mailchimp" : 
	{
		"apiKey" : "",
		"lists" : []
	},
	"payment_settings" : 
	{
		"confirmpayment" : "<center>\n<style type=\"text/css\">\n#docContainer table {width:80%; margin-top: 5px; margin-bottom: 5px;}\n#docContainer td {text-align:right; min-width:25%; font-size: 12px !important; line-height: 20px;margin: 0px;border-bottom: 1px solid #e9e9e9; padding-right:5px;}\n#docContainer td:first-child {text-align:left; font-size: 13px !important; font-weight:bold; vertical-align:text-top; min-width:50%;}\n#docContainer th {font-size: 13px !important; font-weight:bold; vertical-align:text-top; text-align:right; padding-right:5px;}\n#docContainer th:first-child {text-align:left;}\n#docContainer tr:first-child {border-bottom-width:2px; border-bottom-style:solid;}\n#docContainer center {margin-bottom:15px;}\n#docContainer form input { margin:5px; }\n#docContainer #fb_confirm_inline { margin:5px; text-align:center;}\n#docContainer #fb_confirm_inline>center h2 { }\n#docContainer #fb_confirm_inline>center p { margin:5px; }\n#docContainer #fb_confirm_inline>center a { }\n#docContainer #fb_confirm_inline input { border:none; color:transparent; font-size:0px; background-color: transparent; background-repat: no-repeat; }\n#docContainer #fb_paypalwps { background: url('https://coffeecupimages.s3.amazonaws.com/paypal.gif');background-repeat:no-repeat; width:145px; height:42px; }\n#docContainer #fb_authnet { background: url('https://coffeecupimages.s3.amazonaws.com/authnet.gif'); background-repeat:no-repeat; width:135px; height:38px; }\n#docContainer #fb_2checkout { background: url('https://coffeecupimages.s3.amazonaws.com/2co.png'); background-repeat:no-repeat; width:210px; height:44px; }\n#docContainer #fb_invoice { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email.png'); background-repeat:no-repeat; width:102px; height:31px; }\n#docContainer #fb_invoice:hover { background: url('https://coffeecupimages.s3.amazonaws.com/btn_email_hov.png'); }\n#docContainer #fb_goback { color: inherit; }\n</style>\n[_cart_summary_]\n<h2>Almost done! </h2>\n<p>Your order will not be processed until you click the payment button below.</p>\n<a id=\"fb_goback\"href=\"?action=back\">Back to form</a></center>",
		"currencysymbol" : "$",
		"decimals" : 2,
		"fixedprice" : "000",
		"invoicelabel" : "",
		"is_present" : false,
		"paymenttype" : "redirect",
		"shopcurrency" : "USD",
		"usecustomsymbol" : false
	},
	"redirect_settings" : 
	{
		"confirmpage" : "<!DOCTYPE html>\n<html dir=\"ltr\" lang=\"en\">\n<head>\n<title>Success!</title>\n<meta charset=\"utf-8\">\n<style type=\"text/css\">\nbody {background: #f9f9f9;padding-left: 11%;padding-top: 7%; padding-right: 2%;max-width:700px;font-family: Helvetica, Arial;}\ntable{width:80%;}\np{font-size: 16px;font-weight: bold;color: #666;}\nh1{font-size: 60px !important;color: #ccc !important;margin:0px;}\nh2{font-size: 28px !important;color: #666 !important;margin: 0px; border-bottom: 1px dotted #00A2FF; padding-bottom:3px;}\nh3{font-size: 16px; color: #a1a1a1; border-top: 1px dotted #00A2FF; padding-top:1.7%; font-weight: bold;}\nh3 span{color: #ccc;}\ntd {font-size: 12px !important; line-height: 30px;  color: #666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\ntd:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:50%; padding-right:5px;}\na:link {color:#666; text-decoration:none;} a:visited {color:#666; text-decoration:none;} a:hover {color:#00A2FF;}\n</style>\n</head>\n<body>\n<h1>Thanks! </h1>\n<h2>The form is on its way.</h2>\n<p>Here&rsquo;s what was sent:</p>\n<div>[_form_results_]</div>\n<!-- link back to your Home Page -->\n<h3>Let&rsquo;s go <span> <a target=\"_blank\" href=\"http://www.coffeecup.com\">Back Home</a></span></h3>\n</body>\n</html>\n",
		"gotopage" : "",
		"inline" : "<center>\n<style type=\"text/css\">\n#docContainer table {margin-top: 30px; margin-bottom: 30px; width:80%;}\n#docContainer td {font-size: 12px !important; line-height: 30px;color: #666666 !important; margin: 0px;border-bottom: 1px solid #e9e9e9;}\n#docContainer td:first-child {font-size: 13px !important; font-weight:bold; color: #333 !important; vertical-align:text-top; min-width:50%; padding-right:5px;}\n</style>\n[_form_results_]\n<h2>Thank you!</h2><br/>\n<p>Your form was successfully submitted. We received the information shown above.</p>\n</center>",
		"type" : "inline"
	},
	"uid" : "1a6e721cf8270da84bca5eb2ba3ae87f",
	"validation_report" : "in_line"
},
"rules":{"text2":{"label":"Name of Business:","fieldtype":"text","required":true},"text3":{"label":"Name of Contact Person:","fieldtype":"text","required":true},"text4":{"fieldtype":"text","required":true,"label":"City:"},"tel6":{"phone":"phoneUS","label":"Phone Number","fieldtype":"tel","required":true},"email31":{"email":true,"label":"Email for conact person in charge of information needed:","fieldtype":"email"},"select7":{"fieldtype":"dropdown","label":"Choose the Post Season:","required":true,"values":["","Winter Season; Jan 3-Mar 13, 2016","Spring; Mar 20-May 8, 2016","Summer; May 15-Aug14, 2016","Fall; Aug 21-Oct 30, 2016","Winter; Nov 6-Dec 25, 2016"]},"select9":{"fieldtype":"dropdown","label":"Choose the Week of the Post for Winter Season:","required":true,"values":["","Jan 3-9","Jan 10-16","Jan 17-23","Jan 24-30","Jan 31-Feb 6","Feb 7-13","Feb 14-20","Feb 21-27","Feb 28-Mar 5","Mar 6-12","Mar 13-19"]},"select32":{"fieldtype":"dropdown","label":"Choose the Week of the Post for the Spring Season:","required":true,"values":["","Mar 20-Mar 26","Mar 27-Apr 2","Apr 3-Apr 9","Apr 10-Apr 16","Apr 17- Apr 23","Apr 24-Apr 30","May 1-May 7","May 8-May 14"]},"select8":{"fieldtype":"dropdown","label":"Choose the Week of the Post for the Summer Season:","required":true,"values":["","May 15-21","May 22-28","May 29-Jun 4","Jun 5-11","Jun 12-18","Jun 19-25","Jun 26-Jul 2","Jul 3-Jul 9","Jul 10-Jul 16","Jul 17-Jul 23","Jul 24-Jul 30","Jul 31-Aug","Aug 7-13","Aug 14-Aug 20"]},"select10":{"fieldtype":"dropdown","label":"Choose the Week of the Post for the Fall Season:","required":true,"values":["","Aug 21-Aug 27","Aug 28-Sep 3","Sep 4-Sep 10","Sep 11-Sep 17","Sep 18-Sep 24","Sep 25-Oct 1","Oct 2 - Oct 8","Oct 9-Oct 15","Oct 16-Oct 22","Oct 23-Oct 29","Oct 30-Nov 5"]},"select11":{"fieldtype":"dropdown","label":"Choose the Week of the Post for the Winter Season:","required":true,"values":["","Oct 31-Nov 5","Nov 6-Nov 12","Nov 13-Nov 19","Nov 20-Nov 26","Nov 27-Dec 3","Dec 4-Dec 10","Dec 11-Dec 17","Dec 18-Dec 24","Dec 25-Dec 31","Jan 1-Jan 7","Jan 8-Jan 14","Jan 15-Jan 21","Jan 22-Jan 28"]},"text14":{"label":"Headline, limit of 50 characters. This will be used on your post:","fieldtype":"text","required":true},"url15":{"url":true,"label":"Web Address","fieldtype":"url","required":true},"textarea16":{"label":"Description, 2-3 sentences - a limit of 250 characters.","fieldtype":"textarea","maxlength":"300","required":true},"file18":{"label":"Upload a breathtaking photo of your park","accept":"jpg|png|gif|jpeg","files":false,"attach":true,"database":false,"maxbytes":2048000,"fieldtype":"fileupload","required":true},"select20":{"label":"Location:","fieldtype":"dropdown","required":false,"values":["25 Mile Radius","50 Mile Radius","100 Mile Radius"]},"text23":{"label":"City: Choose one major city to target.","fieldtype":"text"},"select27":{"label":"Gender:","fieldtype":"dropdown","required":true,"values":["","Men","Women","Both"]},"text28":{"label":"Age Range, Youngest to Oldest to target:","fieldtype":"text","required":true}},
"payment_rules":{"select7":{},"select9":{},"select32":{},"select8":{},"select10":{},"select11":{},"select20":{},"select27":{}},
"conditional_rules":{"select8":{"element":{"name":"select7","operator":"is","value":"Summer; May 15-Aug14, 2016"}},"select10":{"element":{"name":"select7","operator":"is","value":"Fall; Aug 21-Oct 30, 2016"}},"select32":{"element":{"name":"select7","operator":"is","value":"Spring; Mar 20-May 8, 2016"}},"select9":{"element":{"name":"select7","operator":"is","value":"Winter Season; Jan 3-Mar 13, 2016"}},"select11":{"element":{"name":"select7","operator":"is","value":"Winter; Nov 6-Dec 25, 2016"}}},
"application_version":"Web Form Builder (OSX), build 2.4.5318"
}