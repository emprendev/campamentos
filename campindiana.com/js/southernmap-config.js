﻿var map_config = {
	'default':{
		'landColor':'#EBECED', //general counties color
		'borderColor':'#9CA8B6', //inter-county borders
		'mapShadow':'#000', //shadow color below the map
		'shadowOpacity':'50', //shadow opacity, value: 0-100
		'pinShadow':'#000', //shadow color below the pins
		'pinShadowOpacity':'50', //pins shadow opacity, value: 0-100
		'hoverShadow':'#666666', //the shadow color below the hover popup 
	},
	'points':[
	{
		'shape':'circle',//choose the shape of the pin circle or rectangle
		'hover': '<u><b>Lake Haven Retreat</b></u><br>1951 West Edgewood Ave<br>Indianapolis, IN<br><img src="hover.png" width="196px">',//the content of the hover popup
		'pos_X':200,//location of the pin on X axis
		'pos_Y':216,//location of the pin on Y axis
		'width':5,//width of the pin if rectangle (if circle use diameter)
		'outline':'#FFF',//outline color of the pin
		'thickness':1,//thickness of the line (0 to hide the line)
		'upColor':'#000099',//color of the pin when map loads
		'overColor':'#999900',//color of the pin when mouse hover
		'downColor':'#ffffff',//color of the pin when clicked 
		//(trick, if you make this pin un-clickable > make the overColor and downColor the same)
		'url':'http://www.campindiana.com/lake-haven-retreat.html',//URL of this pin
		'target':'same_window',//'new_window' opens URL in new window//'same_window' opens URL in the same window //'none' pin is not clickable
		'enable':true,//true/false to enable/disable this pin
	},

	
{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Drakes Ridge Rustic Nudist Retreat</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>9641 Drakes Ridge Road<br>Bennington, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-427-3914 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':317,
		'pos_Y':307,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'drakes-ridge-rustic-nudist-retreat.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Lake Monroe Village Resort</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>8107 S. Fairfax Road<br>Bloomington, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-824-2267 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':173,
		'pos_Y':263,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'lake-monroe-village-resort.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Add-More Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>2411 Add-More Lane<br>Clarksville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-283-4321 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':241,
		'pos_Y':355,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'add-more-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Louisville Metro KOA Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>900 Marriott Drive<br>Clarksville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-282-4474 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':241,
		'pos_Y':360,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'louisville-metro-koa.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Ceraland Park</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>3989 S 525 E<br>Columbus, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-377-5849 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':217,
		'pos_Y':263,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'ceraland-park.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Woods-N-Waters Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>8855 S 300 W<br>Columbus, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-342-1619 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':212,
		'pos_Y':267,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'columbus-woods-n-waters.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Moccasin Meadow Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>8941 E CR 400N<br>Greensburg, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-662-9601 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':260,
		'pos_Y':260,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'moccasin-meadow-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Horseshoe Bend RV Campground<br> Cabins & Boat Ramp on the Ohio River</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>850 West Court Street<br>Leavenworth, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-267-3031 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':179,
		'pos_Y':371,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'horseshoe-bend-rv-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Coopers Creek Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>1474 Coopers Bottom Road<br>Milton, KY<br><span style="color: #bcbcbc;">Telephone:</span><br>812-701-5853 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':275,
		'pos_Y':322,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'coopers-creek-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>New Vision RV Park</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>13552 N US Hwy 41, PO Box 209<br>Oaktown, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-745-2125 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':83,
		'pos_Y':324,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'new-vision-rv-park.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Lake Rudolph Campground and RV Resort</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>78 North Holiday Blvd<br>Santa Claus, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>812-937-4458 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':132,
		'pos_Y':375,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'lake-rudolph-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Thorntree Lake Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>7301 N Old US Hwy 421<br>St. Paul, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-604-9261 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':253,
		'pos_Y':249,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'thorntree-lake-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Hidden Paradise Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>802 East Jefferson Street<br>St. Paul, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>765-525-6582 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':251,
		'pos_Y':253,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'hidden-paradise-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	
	]
}
