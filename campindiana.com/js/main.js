/*
 * Portfolio Hover Effect
 * http://www.dxthemes.com/
 *
 * Copyright 2012 DXThemes
 * Free to use under the GPLv2 license.
 * http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Author: Kuzen Leibovitz
 */
$.fn.hoverDescription = function(){
    var ps = portfolioSelector = $(this).children('div');

    if ( ps.length!=0 ) {
        var imgWidth = ps.children('img').width();
        var imgHeight = ps.children('img').height();
        var box = ps.find('.box');
        var koefh = imgHeight - box.find('.project-name').innerHeight() - box.css('padding-top').replace("px", "");
        var blockWidth = imgWidth - box.css('padding-left').replace("px", "") * 2;
        box.css({width:blockWidth});
        box.css({top:koefh});
        var descHeight = imgHeight - box.innerHeight();

        if (ps.children('i').length==0) ps.append("<i></i>");

        ps.hover(function () {
            $(".boxcaption", this).stop().animate({top:descHeight}, {queue:false, duration:500, easing: 'easeOutQuart'});
        }, function () {
            $(".boxcaption", this).stop().animate({top:koefh}, {queue:false, duration:500});
        });
    }
};

$(document).ready(function(){

    // Twitter - sidebar
    // -----------------------------------------------------------------------------
    if ($('#twitter').length) {
        $.getJSON('twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=dxthemes&count=3'), function(tweets){
            $("#twitter").html(tz_format_twitter(tweets));
        });
    } else {}

    // Twitter - footer
    // -----------------------------------------------------------------------------
    if ($('#twitter-foot').length) {
        $.getJSON('twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=dxthemes&count=3'), function(tweets){
            $("#twitter-foot").html(tz_format_twitter(tweets));
        });
    } else { }

    $('.p2col .span6:even').addClass('no-margin-left');

    //prettyPhoto
    $('a[data-rel]').each(function() {
        $(this).attr('rel', $(this).data('rel'));
    });
    $("a[rel^='prettyPhoto']").prettyPhoto();

    //Menu
    jQuery('#menu > ul').superfish({ 
        delay:       1000,                           
        animation:   {opacity:'show', height:'show'}, 
        speed:       'fast',                          
        autoArrows:  true

    });
    $('.sf-sub-indicator').remove();
    (function() {
		var $menu = $('#menu ul'),
			optionsList = '<option value="" selected>Menu...</option>';

		$menu.find('li').each(function() {
			var $this   = $(this),
				$anchor = $this.children('a'),
				depth   = $this.parents('ul').length - 1,
				indent  = '';

			if( depth ) {
				while( depth > 0 ) {
					indent += ' - ';
					depth--;
				}
			}
			optionsList += '<option value="' + $anchor.attr('href') + '">' + indent + ' ' + $anchor.text() + '</option>';
		}).end()
		  .after('<select class="res-menu">' + optionsList + '</select>');

		$('.res-menu').on('change', function() {
			window.location = $(this).val();
		});
		
	})();
    
    $(".our-blog article").hover(function () {
    	$(this).find("img").stop(true, true).animate({ opacity: 0.7 }, 300);
    }, function() {
    	$(this).find("img").stop(true, true).animate({ opacity: 1 }, 300);
    });
    
    
    //Flickr Widget Footer
    $('#footer .flickr').jflickrfeed({
		limit: 6,
		qstrings: {
			id: '36621592@N06'
		},
		itemTemplate: '<li>'+
						'<a rel="prettyPhoto[flickr]" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$("a[rel^='prettyPhoto']").prettyPhoto();

        $("#footer .flickr li").hover(function () {						 
    	   $(this).find("img").stop(true, true).animate({ opacity: 0.5 }, 800);
        }, function() {
    	   $(this).find("img").stop(true, true).animate({ opacity: 1.0 }, 800);
        });
	});


	//Flickr Widget Sidebar
    $('#sidebar .sidebar-flickr').jflickrfeed({
		limit: 8,
		qstrings: {
			id: '36621592@N06'
		},
		itemTemplate: '<li>'+
						'<a rel="prettyPhoto[flickr]" href="{{image}}" title="{{title}}">' +
							'<img src="{{image_s}}" alt="{{title}}" />' +
						'</a>' +
					  '</li>'
	}, function(data) {
		$("a[rel^='prettyPhoto']").prettyPhoto();

        $("#footer .flickr li").hover(function () {						 
    	   $(this).find("img").stop(true, true).animate({ opacity: 0.5 }, 800);
        }, function() {
    	   $(this).find("img").stop(true, true).animate({ opacity: 1.0 }, 800);
        });
	});


	//Portfolio
	var $portfolioClone = $(".filtrable").clone();
	$("#filtrable a").live('click', function(e){
		
		$("#filtrable li").removeClass("current");	
		
		var $filterClass = $(this).parent().attr("class");
        var $filteredPortfolio = $portfolioClone.children("div");

		if ( $filterClass == "all" ) {
			$filteredPortfolio = $portfolioClone.children("div");
		} else {
			$filteredPortfolio = $portfolioClone.children("div[data-type~=" + $filterClass + "]");
		}
	
		$(".filtrable").quicksand( $filteredPortfolio, { 
			duration: 800,
            easing: 'easeInOutQuad'
        }, function(){
            $("a[rel^='prettyPhoto']").prettyPhoto();
            $("#portfolio").hoverDescription();

		});

		$(this).parent().addClass("current");
        
		e.preventDefault();
	});

    // To Top Button
    $(function () {
        $().UItoTop({ easingType:'easeOutQuart' });
    });

});


$(window).load(function() {

	$("#mainslider").flexslider({
		animation: "slide",
		useCSS: false,
		controlNav: false,   
		animationLoop: false,
		smoothHeight: true
	});

    $(function () {
        $(".lightbox-image").append("<span></span>");
        $(".lightbox-image").hover(function () {
            $(this).find("img").stop().animate({opacity:0.5}, "normal")
        }, function () {
            $(this).find("img").stop().animate({opacity:1}, "normal")
        })
    });

    //Projects' Hover Descriptions
    $("#portfolio").hoverDescription();
});

$(window).resize(function() {
    $("#portfolio").hoverDescription();
});
