﻿var map_config = {
	'default':{
		'landColor':'#EBECED', //general counties color
		'borderColor':'#9CA8B6', //inter-county borders
		'mapShadow':'#000', //shadow color below the map
		'shadowOpacity':'50', //shadow opacity, value: 0-100
		'pinShadow':'#000', //shadow color below the pins
		'pinShadowOpacity':'50', //pins shadow opacity, value: 0-100
		'hoverShadow':'#666666', //the shadow color below the hover popup 
	},
	'points':[
	{
		'shape':'circle',//choose the shape of the pin circle or rectangle
		'hover': '<u><b>Lake Haven Retreat</b></u><br>1951 West Edgewood Ave<br>Indianapolis, IN<br><img src="hover.png" width="196px">',//the content of the hover popup
		'pos_X':200,//location of the pin on X axis
		'pos_Y':216,//location of the pin on Y axis
		'width':5,//width of the pin if rectangle (if circle use diameter)
		'outline':'#FFF',//outline color of the pin
		'thickness':1,//thickness of the line (0 to hide the line)
		'upColor':'#000099',//color of the pin when map loads
		'overColor':'#999900',//color of the pin when mouse hover
		'downColor':'#ffffff',//color of the pin when clicked 
		//(trick, if you make this pin un-clickable > make the overColor and downColor the same)
		'url':'http://www.campindiana.com/lake-haven-retreat.html',//URL of this pin
		'target':'same_window',//'new_window' opens URL in new window//'same_window' opens URL in the same window //'none' pin is not clickable
		'enable':true,//true/false to enable/disable this pin
	},

	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Circle B Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>5251 W. US Hwy. 20<br>Angola, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-665-5353 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':316,
		'pos_Y':31,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'circle-b-rv-park.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Ruperts Resort & Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>3408 West Shore Drive<br>Bremen, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-546-2657 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':187,
		'pos_Y':61,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'ruperts-resort-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Ebys Pines RV Park & Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>14583 State Road 120<br>Bristol, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-848-4583 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':233,
		'pos_Y':25,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'ebys-pines-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Acorn Oaks Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>16614 W. SR 114<br>Francesville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>219-567-2524 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':164,
		'pos_Y':92,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'acorn-oaks-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Twin Mills Camping Resort</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>1675 W. SR 120<br>Howe, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-562-3212 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':277,
		'pos_Y':25,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'twin-mills-camping-resort.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Patona Bay Resort</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>162 EMS T33 Lane<br>Leesburg, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-453-3671 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':232,
		'pos_Y':62,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'patona-bay-resort.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Michigan City Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>1601 N. Hwy 421<br>Michigan City, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>219-872-7600 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':145,
		'pos_Y':25,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'michigan-city-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Mini Mountain Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>32351 SR 2<br>New Carlisle, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-654-3307 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':193,
		'pos_Y':26,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'mini-mountain-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Manapogo Park</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>5495 W. 760 N. <br>Orland, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-833-3902 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':316,
		'pos_Y':23,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'manapogo-park.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Caboose Lake Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>3657 W. US 24 <br>Remington, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>219-261-3828 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':110,
		'pos_Y':113,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'caboose-lake-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Hoffman Lake Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>7638 W. CR 300 N <br>Warsaw, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-858-9628 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':226,
		'pos_Y':72,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'hoffman-lake-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Pic-A-Spot Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>6402 East McKenna Road <br>Warsaw, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>574-594-2635 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':236,
		'pos_Y':68,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'pic-a-spot-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Atwood Lake Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>0655 E 800 S <br>Wolcottville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-854-3079 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':273,
		'pos_Y':54,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'atwood-lake-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Gordons Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>9500 E 600 South <br>Wolcottville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-351-3383 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':280,
		'pos_Y':52,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'gordons-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Indian Lakes Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>6460 S 75 W <br>Wolcottville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>260-854-4215 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':267,
		'pos_Y':52,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'indian-lakes-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	]
}
