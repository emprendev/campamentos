﻿var map_config = {
	'default':{
		'landColor':'#EBECED', //general counties color
		'borderColor':'#9CA8B6', //inter-county borders
		'mapShadow':'#000', //shadow color below the map
		'shadowOpacity':'50', //shadow opacity, value: 0-100
		'pinShadow':'#000', //shadow color below the pins
		'pinShadowOpacity':'50', //pins shadow opacity, value: 0-100
		'hoverShadow':'#666666', //the shadow color below the hover popup 
	},
	'points':[
	{
		'shape':'circle',//choose the shape of the pin circle or rectangle
		'hover': '<u><b>Lake Haven Retreat</b></u><br>1951 West Edgewood Ave<br>Indianapolis, IN<br><img src="hover.png" width="196px">',//the content of the hover popup
		'pos_X':200,//location of the pin on X axis
		'pos_Y':216,//location of the pin on Y axis
		'width':5,//width of the pin if rectangle (if circle use diameter)
		'outline':'#FFF',//outline color of the pin
		'thickness':1,//thickness of the line (0 to hide the line)
		'upColor':'#000099',//color of the pin when map loads
		'overColor':'#999900',//color of the pin when mouse hover
		'downColor':'#ffffff',//color of the pin when clicked 
		//(trick, if you make this pin un-clickable > make the overColor and downColor the same)
		'url':'http://www.campindiana.com/lake-haven-retreat.html',//URL of this pin
		'target':'same_window',//'new_window' opens URL in new window//'same_window' opens URL in the same window //'none' pin is not clickable
		'enable':true,//true/false to enable/disable this pin
	},
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Lake Haven Retreat</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>1951 West Edgewood Ave<br>Indianapolis, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-783-5267 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':200,
		'pos_Y':216,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'lake-haven-retreat.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>White River Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>11299 East 234th Street<br>Cicero, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-770-4430 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':219,
		'pos_Y':184,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'white-river-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>SleepyBear Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>13231 E 146th Street<br>Noblesville, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-691-2339 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':220,
		'pos_Y':193,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'sleepybear-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>Heartland RV Resort</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>1613 West 300 N<br>Greenfield, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-326-3181 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':236,
		'pos_Y':215,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'heartland-resort.html',
		'target':'same_window',
		'enable':true,
	},
	
	{
		'shape':'circle',
		'hover': '<span style="text-decoration: underline;"><br><strong>S & H Campground</strong></span><br><span style="color: #bcbcbc;">Street Address:</span><br>2573 W 100 N<br>Greenfield, IN<br><span style="color: #bcbcbc;">Telephone:</span><br>317-326-3208 <br><span style="color: #b9f700;"><strong>"Click dot for Information"</strong></span>',
		'pos_X':232,
		'pos_Y':218,
		'diameter':5,
		'outline':'#FFF',
		'thickness':1,
		'upColor':'#000099',
		'overColor':'#999900',
		'downColor':'#ffffff',
		'url':'s-and-h-campground.html',
		'target':'same_window',
		'enable':true,
	},
	
	
	
	]
}
