 function init_findacampgroundmenuhovers()
 {
   var div      = document.getElementById('findacampgroundmenuhovers');
   if(div)
   {
   var subdiv   = div.childNodes[0];
   var tw  = new Tween(subdiv.style, 'top', Tween.regularEaseInOut, -87, 0, .5, 'px');
   
   div.startf = function ()
  	        {
  	           subdiv.style.top = '-87px';
  	           
                   div.style.overflow = 'hidden';
  	           div.style.display = 'block';
  	           tw.onMotionFinished = function(){ div.style.overflow = 'visible'; tw.onMotionFinished = undefined; }
  	           tw.continueTo(0, .5);
  	           
  	        }
   div.endf  = function ()
  	       { 
                   div.style.overflow = 'hidden';
  	           tw.onMotionFinished = function(){ div.style.display = 'none'; tw.onMotionFinished = undefined; }
  	           tw.continueTo(-87, .5);
  	           
  	       }
   }
}

function init_tweens()
{
 init_findacampgroundmenuhovers();
}